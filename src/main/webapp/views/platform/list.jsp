<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form action="platform/listKeyword1.do" modelAttribute="platform">

<input type="text" name="keyword">
&nbsp;
<input type="submit" name="search" value="<spring:message code="platform.search" />" />
<br></br>
</form:form>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="platforms" requestURI="${requestURI}" id="row">
	
	<spring:message code="platform.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	<spring:message code="platform.description" var="description" />
	<display:column property="description" title="${description}" sortable="true" />

	<spring:message code="platform.satellite" var="satellite" />
	<display:column>
	<a href="satellite/list.do?platformId=${row.id}"> 
	<spring:message	code="platform.satellite" /></a>
	</display:column>
	
	<security:authorize access="isAuthenticated()">
	<display:column>
	<a href="subscription/user/create.do?platformId=${row.id}"> 
	<spring:message	code="platform.subscribe" /></a>
	</display:column>
	</security:authorize>
</display:table>			