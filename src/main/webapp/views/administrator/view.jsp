<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>


<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="agents" requestURI="${requestURI}" id="row">
               
       	<security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="agent/edit.do?agentId=${row.id}"> <spring:message
					code="actor.edit" />
			</a>
		</display:column>
	</security:authorize>           
    
    <spring:message code="agent.ban" var="ban"/>
    <display:column property="ban" title="${ban}" sortable="true"/>
    <spring:message code="agent.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>
    <spring:message code="agent.surname" var="surname"/>
    <display:column property="surname" title="${surname}" sortable="true"/>
    <spring:message code="agent.picture" var="picture"/>
    <display:column property="picture" title="${picture}" sortable="true"/>
    <spring:message code="agent.postalAddress" var="postalAddress"/>
    <display:column property="postalAddress" title="${postalAddress}" sortable="true"/>
    <spring:message code="agent.email" var="email"/>
    <display:column property="email" title="${email}" sortable="true"/>
    <spring:message code="agent.phone" var="phone"/>
    <display:column property="phone" title="${phone}" sortable="true"/>
</display:table>    

<display:table pagesize="15" class="displaytag" keepStatus="true"
               name="handyworkers" requestURI="${requestURI}" id="row">
               
       	<security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="handyworker/edit.do?handyworkerId=${row.id}"> <spring:message
					code="actor.edit" />
			</a>
		</display:column>
	</security:authorize>      
	
	<spring:message code="handyworker.ban" var="ban"/>
    <display:column property="ban" title="${ban}" sortable="true"/>
    <spring:message code="handyworker.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>
    <spring:message code="handyworker.surname" var="surname"/>
    <display:column property="surname" title="${surname}" sortable="true"/>
    <spring:message code="handyworker.picture" var="picture"/>
    <display:column property="picture" title="${picture}" sortable="true"/>
    <spring:message code="handyworker.postalAddress" var="postalAddress"/>
    <display:column property="postalAddress" title="${postalAddress}" sortable="true"/>
    <spring:message code="handyworker.email" var="email"/>
    <display:column property="email" title="${email}" sortable="true"/>
    <spring:message code="handyworker.phone" var="phone"/>
    <display:column property="phone" title="${phone}" sortable="true"/>
    <spring:message code="handyworker.antennaModel" var="antennaModel"/>
    <display:column property="antennaModel" title="${antennaModel}" sortable="true"/>
 </display:table>       
    
    <display:table pagesize="15" class="displaytag" keepStatus="true"
               name="users" requestURI="${requestURI}" id="row">
               
       	<security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="user/edit.do?userId=${row.id}"> <spring:message
					code="actor.edit" />
			</a>
		</display:column>
	</security:authorize>  
    
    <spring:message code="user.ban" var="ban"/>
    <display:column property="ban" title="${ban}" sortable="true"/>
    <spring:message code="user.name" var="name"/>
    <display:column property="name" title="${name}" sortable="true"/>
    <spring:message code="user.surname" var="surname"/>
    <display:column property="surname" title="${surname}" sortable="true"/>
    <spring:message code="user.picture" var="picture"/>
    <display:column property="picture" title="${picture}" sortable="true"/>
    <spring:message code="user.postalAddress" var="postalAddress"/>
    <display:column property="postalAddress" title="${postalAddress}" sortable="true"/>
    <spring:message code="user.email" var="email"/>
    <display:column property="email" title="${email}" sortable="true"/>
    <spring:message code="user.phone" var="phone"/>
    <display:column property="phone" title="${phone}" sortable="true"/>
</display:table>    