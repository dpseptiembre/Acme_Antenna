<%--
 * dashboard.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<script type="text/javascript">
	window.onload = function () {
	   
		var data = [];
		var dataSeries = { type: "line" };
		var dataPoints = ${antennasPerModel};
		dataSeries.dataPoints = dataPoints;
		data.push(dataSeries);
		 
		var options = {
			exportEnabled: true,
			animationEnabled: true,
			title: {
				text: "Antenna"
			},
			axisY: {
				lineThickness: 1
			},
			data: data
		};
		 
		var chart = new CanvasJS.Chart("chartContainer", options);
		chart.render(); 
		}
</script>

<table id="DashBoard" border="1">

	<tr>
		<th><spring:message code="administrator.avgAntennasPerUser" /></th>

		<td><jstl:out value=" ${avgAntennaPerUser}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.standarDesviationOfAntennasPerUser" /></th>

		<td><jstl:out value=" ${standarDesviationOfAntennasPerUser}" /></td>
	</tr>

	<tr>
		<th><spring:message code="administrator.averageQualityOfAntennas" /></th>

		<td><jstl:out value=" ${averageQualityOfAntennas}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.standarDesviationQualityOfAntennas" /></th>

		<td><jstl:out value=" ${standarDesviationQualityOfAntennas}" /></td>
	</tr>

	<tr>
	<tr>

		<th><spring:message code="administrator.antennasPerModel" /></th>

		<td>
			<div id="chartContainer" style="height: 370px; width: 100%;"></div>
    		<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
		</td>
	</tr>

	<tr>

		<th><spring:message code="administrator.top3ModelsOfAntennas" /></th>

		<td><display:table name="top3ModelsOfAntennas" id="row"
				requestURI="administrator/dashboard.do" class="displaytag">

			</display:table></td>

	</tr>

	<tr>
		<th><spring:message
				code="administrator.averageNumberOfTutorialsPerUser" /></th>

		<td><jstl:out value=" ${averageNumberOfTutorialsPerUser}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.standarDesviationOfTutorialsPerUser" /></th>

		<td><jstl:out value=" ${standarDesviationOfTutorialsPerUser}" /></td>
	</tr>


	<tr>
		<th><spring:message
				code="administrator.averageNumberOfCommentsPerTutorial" /></th>

		<td><jstl:out value=" ${averageNumberOfCommentsPerTutorial}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.standarDesviationOfCommentsPerTutorial" /></th>

		<td><jstl:out value=" ${standarDesviationOfCommentsPerTutorial}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.actorsPublishedTutorialAboveAVG" /></th>

		<td><display:table name="actorsPublishedTutorialAboveAVG"
				id="row" requestURI="administrator/dashboard.do" class="displaytag">

				<acme:column code="actor.name" property="name" sortable="true" />
				<acme:column code="actor.email" property="email" sortable="true" />

			</display:table></td>

	</tr>


	<tr>
		<th><spring:message
				code="administrator.averageNumberOfRepliesPerComment" /></th>

		<td><jstl:out value=" ${averageNumberOfRepliesPerComment}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.standarDesviationOfRepliesPerComment" /></th>

		<td><jstl:out value=" ${standarDesviationOfRepliesPerComment}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.averageNumberOfLengthOfComment" /></th>

		<td><jstl:out value=" ${averageNumberOfLengthOfComment}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.standarDesviationOfLengthOfComment" /></th>

		<td><jstl:out value=" ${standarDesviationOfLengthOfComment}" /></td>
	</tr>


	<tr>
		<th><spring:message
				code="administrator.averageNumberOfPicturesPerTutorial" /></th>

		<td><jstl:out value=" ${averageNumberOfPicturesPerTutorial}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.standarDesviationOfPicturesPeroTutorial" /></th>

		<td><jstl:out value=" ${standarDesviationOfPicturesPeroTutorial}" /></td>
	</tr>


	<tr>
		<th><spring:message
				code="administrator.averageNumberOfPicturesPerComment" /></th>

		<td><jstl:out value=" ${averageNumberOfPicturesPerComment}" /></td>
	</tr>

	<tr>
		<th><spring:message
				code="administrator.standarDesviationOfPicturesPeroComment" /></th>

		<td><jstl:out value=" ${standarDesviationOfPicturesPeroComment}" /></td>
	</tr>
	
	

</table>