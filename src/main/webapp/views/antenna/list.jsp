<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<security:authorize access="hasRole('USER')">
	<div>
		<H5>
			<a href="antenna/user/create.do"> <spring:message
					code="antenna.create" />
			</a>
		</H5>
	</div>
</security:authorize>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="antennas" requestURI="${requestURI}" id="row">
	
	<security:authorize access="hasRole('USER')">
		<display:column>
			<a href="antenna/user/edit.do?antennaId=${row.id}"> <spring:message
					code="antenna.edit" />
			</a>
		</display:column>
	</security:authorize>
	
	<spring:message code="antenna.serialNumber" var="serialNumber" />
	<display:column property="serialNumber" title="${serialNumber}" sortable="true" />
	<spring:message code="antenna.model" var="model" />
	<display:column property="model" title="${model}" sortable="true" />
	<spring:message code="antenna.coordinates" var="coordinates" />
	<display:column property="coordinates.longitude" title="${coordinates}" sortable="true" />
	<display:column property="coordinates.latitude" title="${coordinates}" sortable="true" />
	<spring:message code="antenna.azimuth" var="azimuth" />
	<display:column property="azimuth" title="${azimuth}" sortable="true" />
	<spring:message code="antenna.elevation" var="elevation" />
	<display:column property="elevation" title="${elevation}" sortable="true" />
	<spring:message code="antenna.quality" var="quality" />
	<display:column property="quality" title="${quality}" sortable="true" />
	<spring:message code="antenna.satelliteToward" var="satelliteToward" />
	<display:column property="satelliteToward" title="${satelliteToward}" sortable="true" />
	<display:column>
			<a href="http://www.google.com/maps/place/${row.coordinates.longitude},${row.coordinates.latitude}" target="_blank"> <spring:message
					code="antenna.geolocate" />
			</a>
		</display:column>
</display:table>	