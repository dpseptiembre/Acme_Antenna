<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="antenna/user/edit.do" modelAttribute="antenna">

	<form:hidden path="user" />
	<form:hidden path="id" />
	<form:hidden path="version" />

	<acme:textbox path="model" code="antenna.model" />
	<br />
	<acme:textbox path="serialNumber" code="antenna.serialNumber" />
	<br />
	<acme:textbox path="coordinates.longitude"
		code="antenna.coordinates.longitude" type="number" min="-180"
		max="180" step="0.000001" />
	<acme:textbox path="coordinates.latitude"
		code="antenna.coordinates.latitude" type="number" min="-90" max="90"
		step="0.000001" />
	<br />
	<acme:textbox path="azimuth" code="antenna.azimuth" type="number"
		min="0" max="360" step="1" />
	<br />
	<acme:textbox path="elevation" code="antenna.elevation" type="number"
		min="0" max="90" step="1" />
	<br />
	<acme:textbox path="quality" code="antenna.quality" type="number"
		min="0" max="100" step="1" />
	<br />
	<acme:select2 path="satelliteToward" code="antenna.satelliteToward"
		items="${satellites}" itemLabel="name" />
	<br></br>
	<br />

	<!---------------------------- BOTONES -------------------------->



	<input type="submit" name="save"
		value="<spring:message code="antenna.save" />" />

	<input type="submit" name="delete"
		value="<spring:message code="antenna.delete"/>"
		onclick="return confirm('<spring:message code="antenna.confirm.delete" />')" />&nbsp;

    <input type="button" name="cancel"
		value="<spring:message code="antenna.cancel" />"
		onclick="javascript: window.location.replace('antenna/user/list.do')" />

</form:form>