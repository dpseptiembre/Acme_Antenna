<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="comments" requestURI="${requestURI}" id="row">
	<c:set var="localeCode" value="${pageContext.response.locale}" />

	<security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="comment/delete.do?commentId=${row.id}" onclick="return confirm('<spring:message code="comment.confirm.delete"/>') "> 
			<spring:message code="comment.delete" />
			</a>
		</display:column>
	</security:authorize>
	<!-- Attributes -->

	<spring:message code="comment.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />
	<spring:message code="comment.moment" var="moment" />
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="moment" title="${moment}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="moment" title="${moment}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>
	<spring:message code="comment.text" var="text" />
	<display:column property="text" title="${text}" sortable="true" />
	<spring:message code="comment.pictures" var="pictures" />
	<display:column property="pictures" title="${pictures}" sortable="true" />
	
	<security:authorize access="hasRole('USER')">
	<display:column>
		<a href="comment/reply.do?commentId=${row.id}"> <spring:message
				code="comment.reply" />
		</a>
	</display:column>
	</security:authorize>
	
	<display:column>
		<a href="comment/list.do?commentId=${row.id}"> <spring:message
				code="comment.list.reply" />
		</a>
	</display:column>

</display:table>
