<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.security.Principal" %> 
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>


<security:authorize access="isAuthenticated()">
	<div>
		<H5>
			<a href="tutorial/create.do"> <spring:message
					code="tutorial.create" />
			</a>
		</H5>
	</div>
</security:authorize>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="tutorials" requestURI="${requestURI}" id="row">
	<c:set var="localeCode" value="${pageContext.response.locale}" />
	
	<security:authorize access="hasRole('ADMIN')">
	<display:column>
			<a href="tutorial/delete.do?tutorialId=${row.id}" onclick="return confirm('<spring:message code="tutorial.confirm.delete"/>') "> 
			<spring:message code="tutorial.delete" />
			</a>
		</display:column>
	</security:authorize>
	
	<spring:message code="tutorial.moment" var="moment" />
	
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="moment" title="${moment}" sortable="true" format="{0,date,yyyy/MM/dd}"/>
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="moment" title="${moment}" sortable="true" format="{0,date,dd-MM-yyyy}"/>
		</c:when>
	</c:choose>
	
	
	
	<spring:message code="tutorial.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />
	<spring:message code="tutorial.text" var="text" />
	<display:column property="text" title="${text}" sortable="true" />
	<spring:message code="tutorial.pictures" var="pictures" />
	<display:column property="pictures" title="${pictures}" sortable="true" />
	
	  <security:authorize access="isAuthenticated()">
		<display:column>
	        <a href="tutorial/view.do?tutorialId=${row.id}" class="btn btn-primary">
	            <spring:message code="tutorial.comments"/>
	        </a>
    	</display:column>
    	</security:authorize>
    	<security:authorize access="hasRole('USER')">
    	<display:column>
	        <a href="comment/create.do?tutorialId=${row.id}" class="btn btn-primary">
	            <spring:message code="tutorial.create.comment"/>
	        </a>
    	</display:column>
		<jstl:if test="${row.user.id == principal.id}">
		<display:column>
			<a href="tutorial/edit.do?tutorialId=${row.id}"> <spring:message
					code="tutorial.edit" />
			</a>
		</display:column>
	</jstl:if>
	</security:authorize>
	
</display:table>