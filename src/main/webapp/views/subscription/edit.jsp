<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>



<form:form action="subscription/user/edit.do" modelAttribute="subscription">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="platform"/>
    <form:hidden path="user"/>
    <form:hidden path="keyCode"/>

    <acme:textbox path="startDate" code="subscription.startDate" type="Date"/>
    <br />
    <acme:textbox path="endDate" code="subscription.endDate" type="Date"/>
    <br />
    <acme:textbox path="creditCard.holderName" code="subscription.creditCard.holderName"/>
    <br />
    <acme:textbox path="creditCard.brandName" code="subscription.creditCard.brandName"/>
    <br />
    <acme:textbox path="creditCard.number" code="subscription.creditCard.number"/>
    <br />
    <acme:textbox path="creditCard.expirationYear" code="subscription.creditCard.expirationYear"/>
    <br />
    <acme:textbox path="creditCard.CVV" code="subscription.creditCard.CVV"/>
    <br />
    <acme:textbox path="creditCard.expirationMonth" code="subscription.creditCard.expirationMonth"/>
    <br />
    
        <!---------------------------- BOTONES -------------------------->



    <input type="submit" name="save"
           value="<spring:message code="subscription.save" />" />


    <input type="button" name="cancel"
           value="<spring:message code="subscription.cancel" />"
           onclick="javascript: window.location.replace('subscription/user/list.do')" />

</form:form>