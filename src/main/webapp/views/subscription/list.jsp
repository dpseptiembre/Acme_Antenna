<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="subscriptions" requestURI="${requestURI}" id="row">
	<c:set var="localeCode" value="${pageContext.response.locale}" />
	
	<acme:column code="subscription.platform.name" property="platform.name" sortable="true"/>
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<acme:column code="subscription.startDate" property="startDate" sortable="true" format="{0,date,yyyy/MM/dd}"/>
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<acme:column code="subscription.startDate" property="startDate" sortable="true" format="{0,date,dd-MM-yyyy}"/>
		</c:when>
	</c:choose>
	
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<acme:column code="subscription.endDate" property="endDate" sortable="true" format="{0,date,yyyy/MM/dd}"/>
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<acme:column code="subscription.endDate" property="endDate" sortable="true" format="{0,date,dd-MM-yyyy}"/>
		</c:when>
	</c:choose>

	
	
	<acme:column code="subscription.keyCode" property="keyCode" sortable="true"/>
	
</display:table>	