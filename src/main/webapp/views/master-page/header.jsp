<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Acme-Antenna Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv"><spring:message	code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="administrator/dashboard.do"><spring:message code="master.page.administrator.dashboard" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('USER')">

			<li><a class="fNiv"><spring:message	code="master.page.user" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="antenna/user/list.do"><spring:message code="master.page.antenna.user.list" /></a></li>
					<li><a href="antenna/user/create.do"><spring:message code="master.page.antenna.user.create" /></a></li>		
					<li><a href="subscription/user/list.do"><spring:message code="master.page.subscription.user.list" /></a></li>
					</ul>
			</li>

		</security:authorize>
		
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li><a href="user/create.do"><spring:message code="master.page.user.register" /></a></li>
			<li><a href="satellite/list.do"><spring:message code="master.page.satellite.list" /></a></li>
			<li><a href="platform/list.do"><spring:message code="master.page.platform.list" /></a></li>
			<li><a href="tutorial/list2.do"><spring:message code="master.page.tutorial.list" /></a></li>

		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
			<li><a href="satellite/list.do"><spring:message code="master.page.satellite.list" /></a></li>
			<li><a href="platform/list.do"><spring:message code="master.page.platform.list" /></a></li>
			<li><a href="tutorial/list.do"><spring:message code="master.page.tutorial.list" /></a></li>
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasRole('USER')">
						<!-- <li><a href="user/edit.do"><spring:message code="master.page.user.edit" /></a></li> -->
					</security:authorize>
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

