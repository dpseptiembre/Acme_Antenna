<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form action="satellite /listKeyword1.do" modelAttribute="satellite">

<input type="text" name="keyword">
&nbsp;
<input type="submit" name="search" value="<spring:message code="satellite.search" />" />
<br></br>
</form:form>



<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="satellites" requestURI="${requestURI}" id="row">
	
	<spring:message code="satellite.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	<spring:message code="satellite.description" var="description" />
	<display:column property="description" title="${description}" sortable="true" />
	<spring:message code="satellite.platforms" var="platformsHeader" />
	<display:column>
	<a href="platform/list.do?satelliteId=${row.id}"> 
	<spring:message	code="satellite.platforms" /></a>
	</display:column>
</display:table>			