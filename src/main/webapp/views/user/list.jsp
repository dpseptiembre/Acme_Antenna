<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
	name="users" requestURI="${requestURI}" id="row">
	<spring:message code="user.name" var="name" />
	<display:column property="name" title="${name}" sortable="true" />
	<spring:message code="user.surname" var="surname" />
	<display:column property="surname" title="${surname}" sortable="true" />
	<spring:message code="user.picture" var="picture" />
	<display:column property="picture" title="${picture}" sortable="true" />
	<spring:message code="user.postalAddress" var="postalAddress" />
	<display:column property="postalAddress" title="${postalAddress}"
		sortable="true" />
	<spring:message code="user.email" var="email" />
	<display:column property="email" title="${email}" sortable="true" />
	<spring:message code="user.phone" var="phone" />
	<display:column property="phone" title="${phone}" sortable="true" />
</display:table>
