<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">
<form:form action="user/edit.do" modelAttribute="user">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="picture"/>
    <form:hidden path="email"/>
    <form:hidden path="phone"/>
    <form:hidden path="postalAddress"/>
    <form:hidden path="userAccount"/>

    <acme:textbox path="name" code="user.name"/>
    <acme:textbox path="surname" code="user.surname"/>
    <acme:textbox path="ban"  code="user.ban"/>

    <br>

    <!---------------------------- BOTONES -------------------------->

    <acme:submit name="save" code="general.save"/>
    <input type="button" class="btn btn-danger" name="cancel"
           value="<spring:message code="general.cancel" />"
           onclick="relativeRedir('welcome/index.do');"/>
    <br/>

</form:form>

</div>
