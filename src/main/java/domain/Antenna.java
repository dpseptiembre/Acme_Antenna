
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Access(AccessType.PROPERTY)
public class Antenna extends DomainEntity {

	public Antenna() {
		super();
	}


	private String		serialNumber;
	private String		model;
	private Coordinates	coordinates;
	private Integer		azimuth;
	private Integer		elevation;
	private Integer		quality;
	private String		satelliteToward;

	private User		user;


	@NotBlank
	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(final String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getModel() {
		return this.model;
	}

	public void setModel(final String model) {
		this.model = model;
	}

	@Valid
	public Coordinates getCoordinates() {
		return this.coordinates;
	}

	public void setCoordinates(final Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	@Range(min = 0, max = 360)
	@NotNull
	public Integer getAzimuth() {
		return this.azimuth;
	}

	public void setAzimuth(final Integer azimuth) {
		this.azimuth = azimuth;
	}

	@Range(min = 0, max = 90)
	@NotNull
	public Integer getElevation() {
		return this.elevation;
	}

	public void setElevation(final Integer elevation) {
		this.elevation = elevation;
	}

	@Range(min = 0, max = 100)
	@NotNull
	public Integer getQuality() {
		return this.quality;
	}

	public void setQuality(final Integer quality) {
		this.quality = quality;
	}

	@ManyToOne
	public User getUser() {
		return this.user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getSatelliteToward() {
		return this.satelliteToward;
	}

	public void setSatelliteToward(final String satelliteToward) {
		this.satelliteToward = satelliteToward;
	}

}
