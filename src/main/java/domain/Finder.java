
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Access(AccessType.PROPERTY)
public class Finder extends DomainEntity {

	// Attributes ------------------------------------------------------------

	private String					keyword;
	private Collection<Satellite>	satellites;
	private Collection<Platform>	platforms;


	// Relationships ---------------------------------------------------------

	// Constructors -----------------------------------------------------------
	public Finder() {
		super();
	}

	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getKeyword() {
		return this.keyword;
	}

	public void setKeyword(final String keyword) {
		this.keyword = keyword;
	}

	@OneToMany
	public Collection<Satellite> getSatellites() {
		return this.satellites;
	}

	public void setSatellites(final Collection<Satellite> satellites) {
		this.satellites = satellites;
	}

	@OneToMany
	public Collection<Platform> getPlatforms() {
		return this.platforms;
	}

	public void setPlatforms(final Collection<Platform> platforms) {
		this.platforms = platforms;
	}

}
