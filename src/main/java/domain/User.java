
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor {

	public User() {
		super();
	}


	private Subscription			subscription;
	private Collection<Antenna>		antennas;
	private Collection<Tutorial>	tutorials;
	private Collection<Comment>		comments;


	@OneToOne
	public Subscription getSubscription() {
		return this.subscription;
	}

	public void setSubscription(final Subscription subscription) {
		this.subscription = subscription;
	}

	@OneToMany(mappedBy = "user")
	public Collection<Antenna> getAntennas() {
		return this.antennas;
	}

	public void setAntennas(final Collection<Antenna> antennas) {
		this.antennas = antennas;
	}

	@OneToMany(mappedBy = "user")
	public Collection<Tutorial> getTutorials() {
		return this.tutorials;
	}

	public void setTutorials(final Collection<Tutorial> tutorials) {
		this.tutorials = tutorials;
	}
	@OneToMany
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

}
