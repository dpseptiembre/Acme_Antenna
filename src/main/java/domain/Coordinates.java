
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Range;

@Embeddable
@Access(AccessType.PROPERTY)
public class Coordinates {

	private Double	longitude;
	private double	latitude;


	@Digits(integer = 3, fraction = 6)
	@Range(min = (long) -180.0, max = (long) 180.0)
	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(final Double longitude) {
		this.longitude = longitude;
	}

	@Digits(integer = 3, fraction = 6)
	@Range(min = (long) -90.0, max = (long) 90.0)
	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(final double latitude) {
		this.latitude = latitude;
	}

}
