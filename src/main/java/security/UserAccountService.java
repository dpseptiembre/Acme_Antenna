/*
 * UserAccountService.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Administrator;
import domain.User;

@Service
@Transactional
public class UserAccountService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private UserAccountRepository	userAccountRepository;


	// Supporting services ----------------------------------------------------

	// Constructors -----------------------------------------------------------

	public UserAccountService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------
	public UserAccount save(final UserAccount userAccount) {
		Assert.notNull(userAccount);
		return this.userAccountRepository.save(userAccount);
	}

	public UserAccount findByActor(final Actor actor) {
		Assert.notNull(actor);

		UserAccount result;

		result = this.userAccountRepository.findByActorId(actor.getId());

		return result;
	}

	public UserAccount findByAdministrator(final Administrator administrator) {
		Assert.notNull(administrator);

		UserAccount result;

		result = this.userAccountRepository.findByAdministratorId(administrator.getId());

		return result;
	}

	public UserAccount findByUser(final User user) {
		Assert.notNull(user);

		UserAccount result;

		result = this.userAccountRepository.findByUserId(user.getId());

		return result;
	}


	// Other business methods -------------------------------------------------

}
