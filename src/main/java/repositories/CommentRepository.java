
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	@Query("select distinct c from Comment c where c.title like %?1% or c.text like %?1%")
	Collection<Comment> findAllWithTabooWord(String tabooWord);

}
