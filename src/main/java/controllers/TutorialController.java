
package controllers;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.TutorialService;
import domain.Actor;
import domain.Tutorial;
import services.UserService;

@Controller
@RequestMapping("/tutorial")
public class TutorialController extends AbstractController {

	public TutorialController() {
		super();
	}


	@Autowired
	private TutorialService	tutorialService;
	@Autowired
	private ActorService	actorService;
	@Autowired
	private UserService userService;


	protected static ModelAndView createEditModelAndView(final Tutorial tutorial) {
		ModelAndView result;

		result = TutorialController.createEditModelAndView(tutorial, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Tutorial tutorial, final String message) {
		ModelAndView result;

		result = new ModelAndView("tutorial/edit");
		result.addObject("tutorial", tutorial);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView tutorialsList() {

		ModelAndView result;
		Collection<Tutorial> tutorials;
		Actor principal;
		tutorials = this.tutorialService.findAll();
		principal = this.actorService.findByPrincipal();
		result = new ModelAndView("tutorial/list");
		result.addObject("tutorials", tutorials);
		result.addObject("requestURI", "tutorial/list.do");
		result.addObject("principal", principal);

		return result;
	}

	@RequestMapping(value = "/list2", method = RequestMethod.GET)
	public ModelAndView tutorialsList2() {

		ModelAndView result;
		Collection<Tutorial> tutorials;
		tutorials = this.tutorialService.findAll();
		result = new ModelAndView("tutorial/list");
		result.addObject("tutorials", tutorials);
		result.addObject("requestURI", "tutorial/list.do");

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;

		final Tutorial tutorial = this.tutorialService.create();
		result = TutorialController.createEditModelAndView(tutorial);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int tutorialId) {
		ModelAndView result;

		Tutorial tutorial;
		tutorial = this.tutorialService.findOne(tutorialId);
		Assert.notNull(tutorial);
		Assert.isTrue(userService.findByPrincipal().getTutorials().contains(tutorial));
		result = TutorialController.createEditModelAndView(tutorial);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Tutorial tutorial, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = TutorialController.createEditModelAndView(tutorial);
		else
			try {
				tutorial.setMoment(new Date(System.currentTimeMillis() - 1000));
				this.tutorialService.save(tutorial);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = TutorialController.createEditModelAndView(tutorial, "tutorial.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Tutorial tutorial) {
		ModelAndView result;
		try {
			this.tutorialService.delete(tutorial);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = TutorialController.createEditModelAndView(tutorial, "tutorial.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam final int tutorialId) {
		ModelAndView result;
		try {
			final Tutorial tutorial = this.tutorialService.findOne(tutorialId);
			result = new ModelAndView("tutorial/view");
			result.addObject("name", tutorial.getTitle());
			result.addObject("comments", tutorial.getComments());
		} catch (final Throwable oops) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", oops.getMessage());
			return result;
		}

		return result;
	}

	@RequestMapping(value = "/terms", method = RequestMethod.GET)
	public ModelAndView terms() {
		ModelAndView result;

		result = new ModelAndView("tutorial/terms");

		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView tutorialDelete(@RequestParam final int tutorialId) {

		ModelAndView result;
		Tutorial tutorial;

		tutorial = this.tutorialService.findOne(tutorialId);
		Assert.isTrue(userService.findByPrincipal().getTutorials().contains(tutorial));
		this.tutorialService.delete(tutorial);

		Collection<Tutorial> tutorials;
		tutorials = this.tutorialService.findAll();
		result = new ModelAndView("tutorial/list");
		result.addObject("tutorials", tutorials);
		result.addObject("requestURI", "tutorial/list.do");

		return result;
	}

}
