package controllers.administrator;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


import controllers.AbstractController;

import domain.Tutorial;

@Controller
@RequestMapping("/tutorial/administrator")
public class TutorialAdministratorController extends AbstractController {
	
	// Services ----------------------------------------------------------------



		// Constructors----------------------------------------------

		public TutorialAdministratorController() {
			super();
		}

		protected ModelAndView createEditModelAndView(final Tutorial tutorial) {
			ModelAndView result;

			result = createEditModelAndView(tutorial, null);

			return result;
		}

		protected ModelAndView createEditModelAndView(final Tutorial tutorial,
				final String message) {
			ModelAndView result;

			result = new ModelAndView("tutorial/edit");
			result.addObject("tutorial", tutorial);
			result.addObject("message", message);

			return result;
		}
	


}
