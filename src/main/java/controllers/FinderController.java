
package controllers;

import java.util.Collection;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.FinderService;
import domain.Finder;
import domain.Platform;
import domain.Satellite;

@Controller
@RequestMapping("/finder")
public class FinderController extends AbstractController {

	//Services ----------------------------------------------------------------

	@Autowired
	private FinderService	finderService;


	//Constructors----------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView finderList() {

		ModelAndView result;
		Collection<Finder> finders;

		finders = this.finderService.findAll();
		result = new ModelAndView("finder/list");
		result.addObject("finders", finders);
		result.addObject("requestURI", "finder/list.do");

		return result;
	}

	//Create Method -----------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;

		final Finder finder = this.finderService.create();
		result = this.createEditModelAndView(finder);

		return result;

	}

	// Edition ---------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int finderId) {
		ModelAndView result;
		Finder finder;

		finder = this.finderService.findOne(finderId);
		Assert.notNull(finder);
		result = this.createEditModelAndView(finder);

		return result;
	}

	@RequestMapping(value = "/find", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Finder finder, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(finder);
		else
			try {

				final Set<Satellite> satellites = this.finderService.finderSatellite(finder.getKeyword());
				final Set<Platform> platforms = this.finderService.finderPlatform(finder.getKeyword());
				this.finderService.save(finder);
				result = new ModelAndView("platform/list");
				result.addObject("satellites", satellites);
				result.addObject("platforms", platforms);
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(finder, "finder.commit.error");
			}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Finder finder) {
		ModelAndView result;
		try {
			this.finderService.delete(finder);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(finder, "finder.commit.error");
		}

		return result;
	}

	// Ancillary methods ------------------------------------------------

	protected ModelAndView createEditModelAndView(final Finder finder) {
		ModelAndView result;

		result = this.createEditModelAndView(finder, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Finder finder, final String message) {
		ModelAndView result;

		result = new ModelAndView("finder/edit");
		result.addObject("finder", finder);
		result.addObject("message", message);

		return result;

	}

}
