
package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SatelliteService;
import domain.Satellite;

@Controller
@RequestMapping("/satellite")
public class SatelliteController extends AbstractController {

	public SatelliteController() {
		super();
	}


	@Autowired
	private SatelliteService	satelliteService;



	protected static ModelAndView createEditModelAndView(final Satellite satellite) {
		ModelAndView result;

		result = SatelliteController.createEditModelAndView(satellite, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Satellite satellite, final String message) {
		ModelAndView result;

		result = new ModelAndView("satellite/edit");
		result.addObject("satellite", satellite);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView satellitesList(final Integer satelliteId) {

		ModelAndView result;
		Collection<Satellite> satellites;

		if (satelliteId != null)
			satellites = this.satelliteService.findByPlatformId(satelliteId);
		else
			satellites = this.satelliteService.findAll();
		result = new ModelAndView("satellite/list");
		result.addObject("satellites", satellites);
		result.addObject("requestURI", "satellite/list.do");
		
		return result;
	}

	@RequestMapping(value = "/listKeyword1", method = RequestMethod.POST)
	public ModelAndView listKeyword1(@RequestParam final String keyword) {

		ModelAndView result;
		Collection<Satellite> satellites;
		try {
			satellites = this.satelliteService.findByKeyword(keyword);
			result = new ModelAndView("satellite/list");
			result.addObject("requestURI", "satellite/listKeyword.do");
			result.addObject("satellites", satellites);
			
		} catch (final Throwable error) {
			result = new ModelAndView("redirect:list.do");
			
		}
		return result;
	}

}
