/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdministratorService;
import services.UserService;
import domain.Actor;

import domain.User;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}

	@Autowired
	private AdministratorService administratorService;

	@Autowired
	private UserService userService;

	@Autowired
	private ActorService actorService;
	

	// Action-1 ---------------------------------------------------------------

	@RequestMapping("/action-1")
	public ModelAndView action1() {
		ModelAndView result;

		result = new ModelAndView("administrator/action-1");

		return result;
	}

	// Action-2 ---------------------------------------------------------------

	@RequestMapping("/action-2")
	public ModelAndView action2() {
		ModelAndView result;

		result = new ModelAndView("administrator/action-2");

		return result;
	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard() {

		ModelAndView result;

		try {
			final Double avgAntennaPerUser = this.administratorService
					.avgAntennaPerUser();
			final Double standarDesviationOfAntennasPerUser = this.administratorService
					.standarDesviationOfAntennasPerUser();
			final Double averageQualityOfAntennas = this.administratorService
					.averageQualityOfAntennas();
			final Double standarDesviationQualityOfAntennas = this.administratorService
					.standarDesviationQualityOfAntennas();
			final String dataobject = this.administratorService.createChart();
			final Collection<String> top3ModelsOfAntennas = this.administratorService
					.top3ModelsOfAntennas();
			final Double averageNumberOfTutorialsPerUser = this.administratorService
					.averageNumberOfTutorialsPerUser();
			final Double standarDesviationOfTutorialsPerUser = this.administratorService
					.standarDesviationOfTutorialsPerUser();
			final Double averageNumberOfCommentsPerTutorial = this.administratorService
					.averageNumberOfCommentsPerTutorial();
			final Double standarDesviationOfCommentsPerTutorial = this.administratorService
					.standarDesviationOfCommentsPerTutorial();
			final Collection<Actor> actorsPublishedTutorialAboveAVG = this.administratorService
					.actorsPublishedTutorialAboveAVG();
			final Double averageNumberOfRepliesPerComment = this.administratorService
					.averageNumberOfRepliesPerComment();
			final Double standarDesviationOfRepliesPerComment = this.administratorService
					.standarDesviationOfRepliesPerComment();
			final Double averageNumberOfLengthOfComment = this.administratorService
					.averageNumberOfLengthOfComment();
			final Double standarDesviationOfLengthOfComment = this.administratorService
					.standarDesviationOfLengthOfComment();
			final Double averageNumberOfPicturesPerTutorial = this.administratorService
					.averageNumberOfPicturesPerTutorial();
			final Double standarDesviationOfPicturesPeroTutorial = this.administratorService
					.standarDesviationOfPicturesPeroTutorial();
			final Double averageNumberOfPicturesPerComment = this.administratorService
					.averageNumberOfPicturesPerComment();
			final Double standarDesviationOfPicturesPeroComment = this.administratorService
					.standarDesviationOfPicturesPeroComment();

			result = new ModelAndView("administrator/dashboard");

			result.addObject("avgAntennaPerUser", avgAntennaPerUser);
			result.addObject("standarDesviationOfAntennasPerUser",
					standarDesviationOfAntennasPerUser);
			result.addObject("averageQualityOfAntennas",
					averageQualityOfAntennas);
			result.addObject("standarDesviationQualityOfAntennas",
					standarDesviationQualityOfAntennas);
			result.addObject("antennasPerModel", dataobject);
			result.addObject("top3ModelsOfAntennas", top3ModelsOfAntennas);
			result.addObject("averageNumberOfTutorialsPerUser",
					averageNumberOfTutorialsPerUser);
			result.addObject("standarDesviationOfTutorialsPerUser",
					standarDesviationOfTutorialsPerUser);
			result.addObject("averageNumberOfCommentsPerTutorial",
					averageNumberOfCommentsPerTutorial);
			result.addObject("standarDesviationOfCommentsPerTutorial",
					standarDesviationOfCommentsPerTutorial);
			result.addObject("actorsPublishedTutorialAboveAVG",
					actorsPublishedTutorialAboveAVG);
			result.addObject("averageNumberOfRepliesPerComment",
					averageNumberOfRepliesPerComment);
			result.addObject("standarDesviationOfRepliesPerComment",
					standarDesviationOfRepliesPerComment);
			result.addObject("averageNumberOfLengthOfComment",
					averageNumberOfLengthOfComment);
			result.addObject("standarDesviationOfLengthOfComment",
					standarDesviationOfLengthOfComment);
			result.addObject("averageNumberOfPicturesPerTutorial",
					averageNumberOfPicturesPerTutorial);
			result.addObject("standarDesviationOfPicturesPeroTutorial",
					standarDesviationOfPicturesPeroTutorial);
			result.addObject("averageNumberOfPicturesPerComment",
					averageNumberOfPicturesPerComment);
			result.addObject("standarDesviationOfPicturesPeroComment",
					standarDesviationOfPicturesPeroComment);
			

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		
		return result;

	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view() {
		ModelAndView result;
		final Collection<User> users = this.userService.findAll();

		result = new ModelAndView("administrator/view");
		result.addObject("users", users);
		result.addObject("requestURI", "administrator/view.do");

		return result;
	}

	@RequestMapping(value = "/unban", method = RequestMethod.GET)
	public ModelAndView unban(final int actorId) {

		ModelAndView result;
		final Actor actor = this.actorService.findOne(actorId);
		this.administratorService.unban(actor);

		final Collection<Actor> actors = this.actorService.findAllExceptAdmin();
		result = new ModelAndView("actor/list");
		result.addObject("actors", actors);
		result.addObject("requestURI", "administrator/actorList.do");

		return result;

	}

	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public ModelAndView userBan(final int actorId) {

		ModelAndView result;
		final Actor actor = this.actorService.findOne(actorId);
		this.administratorService.ban(actor);
		final Collection<Actor> actors = this.actorService.findAllExceptAdmin();
		result = new ModelAndView("actor/list");
		result.addObject("actors", actors);
		result.addObject("requestURI", "administrator/actorList.do");

		return result;
	}

	@RequestMapping(value = "/actorList", method = RequestMethod.GET)
	public ModelAndView actorList() {

		ModelAndView result;
		final Collection<Actor> actors = this.actorService.findAllExceptAdmin();

		result = new ModelAndView("actor/list");
		result.addObject("actors", actors);
		result.addObject("requestURI", "administrator/actorList.do");

		return result;
	}
}
