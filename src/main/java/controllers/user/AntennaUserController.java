package controllers.user;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AntennaService;
import services.SatelliteService;
import services.UserService;

import controllers.AbstractController;
import domain.Antenna;
import domain.Satellite;
import domain.User;

@Controller
@RequestMapping("/antenna/user")
public class AntennaUserController extends AbstractController {

	// Services ----------------------------------------------------------------

	@Autowired
	private AntennaService antennaService;
	@Autowired
	private UserService userService;
	@Autowired
	private SatelliteService satelliteService;


	// Constructors----------------------------------------------

	public AntennaUserController() {
		super();
	}
	
	protected ModelAndView createEditModelAndView(final Antenna antenna) {
		ModelAndView result;

		result = createEditModelAndView(antenna, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Antenna antenna, final String message) {
		ModelAndView result;
		Collection<Satellite> satellites;

		satellites= satelliteService.findAll();
		result = new ModelAndView("antenna/edit");
		result.addObject("antenna", antenna);
		result.addObject("message", message);
		result.addObject("satellites", satellites);

		return result;
	}

	

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView antennasList() {

		ModelAndView result;
		Collection<Antenna> antennas;
		User user;
		user= userService.findByPrincipal();
		antennas = this.antennaService.findByUserId(user.getId());
		result = new ModelAndView("antenna/list");
		result.addObject("antennas", antennas);
		result.addObject("requestURI", "antenna/user/list.do");
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;

		final Antenna antenna = this.antennaService.create();
		result = createEditModelAndView(antenna);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int antennaId) {
		ModelAndView result;
		Antenna antenna;

		antenna = this.antennaService.findOne(antennaId);
		Assert.notNull(antenna);
		Assert.isTrue(userService.findByPrincipal().getAntennas().contains(antenna));
		result = createEditModelAndView(antenna);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Antenna antenna, final BindingResult binding) {
		ModelAndView result;
		User user;
		user=userService.findByPrincipal();
		Assert.isTrue(user.getId()==antenna.getUser().getId());
		if (binding.hasErrors())
			result = createEditModelAndView(antenna);
		else
			try {
				this.antennaService.save(antenna);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = createEditModelAndView(antenna, "antenna.commit.error");
			}
		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int antennaId) {
		ModelAndView result;
		User user;
		user=userService.findByPrincipal();
		
		try {
			final Antenna antenna = this.antennaService.findOne(antennaId);
			Assert.isTrue(user.getId()==antenna.getUser().getId());
			this.antennaService.delete(antenna);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			final Antenna antenna = this.antennaService.findOne(antennaId);
			result = createEditModelAndView(antenna, "antenna.commit.error");
		}
		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Antenna antenna) {
		ModelAndView result;
		User user;
		user=userService.findByPrincipal();
		Assert.isTrue(user.getId()==antenna.getUser().getId());
		try {
			this.antennaService.delete(antenna);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = createEditModelAndView(antenna, "antenna.commit.error");
		}
		return result;
	}

}
