package controllers.user;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import services.PlatformService;
import services.SubscriptionService;

import services.UserService;
import controllers.AbstractController;
import domain.Platform;
import domain.Subscription;

import domain.User;


@Controller
@RequestMapping("/subscription/user")
public class SubscriptionUserController extends AbstractController {

	// Services ----------------------------------------------------------------

	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PlatformService platformService;

	// Constructors----------------------------------------------

	public SubscriptionUserController() {
		super();
	}
	
	protected ModelAndView createEditModelAndView(final Subscription subscription) {
		ModelAndView result;

		result = createEditModelAndView(subscription, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Subscription subscription, final String message) {
		ModelAndView result;

		result = new ModelAndView("subscription/edit");
		result.addObject("subscription", subscription);
		result.addObject("message", message);

		return result;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView subscriptionsList() {

		ModelAndView result;
		Collection<Subscription> subscriptions;
		User user;
		user= userService.findByPrincipal();
		subscriptions = this.subscriptionService.findByUserId(user.getId());
		result = new ModelAndView("subscription/list");
		result.addObject("subscriptions", subscriptions);
		result.addObject("requestURI", "subscription/user/list.do");
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int platformId) {

		ModelAndView result;
		Platform platform;
		
		final Subscription subscription = this.subscriptionService.create();
		platform= platformService.findOne(platformId);
		subscription.setPlatform(platform);
		
		
		result = createEditModelAndView(subscription);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int subscriptionId) {
		ModelAndView result;
		Subscription subscription;

		subscription = this.subscriptionService.findOne(subscriptionId);
		Assert.notNull(subscription);
		Assert.isTrue(subscription.getUser().equals(userService.findByPrincipal()));
		result = createEditModelAndView(subscription);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Subscription subscription, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = createEditModelAndView(subscription);
		else
			try {
				this.subscriptionService.save(subscription);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = createEditModelAndView(subscription, "subscription.commit.error");
			}
		return result;
	}
	
}