package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import services.PlatformService;
import domain.Platform;

@Controller
@RequestMapping("/platform")
public class PlatformController extends AbstractController {

	public PlatformController() {
		super();
	}

	@Autowired
	private PlatformService platformService;


	protected static ModelAndView createEditModelAndView(final Platform platform) {
		ModelAndView result;

		result = PlatformController.createEditModelAndView(platform, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(
			final Platform platform, final String message) {
		ModelAndView result;

		result = new ModelAndView("platform/edit");
		result.addObject("platform", platform);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView platformsList(Integer satelliteId) {
		ModelAndView result;
		Collection<Platform> platforms;
		if (satelliteId != null) {
			platforms = this.platformService.findBySatelliteId(satelliteId);
		} else {
			platforms = this.platformService.findAll();
		}
		result = new ModelAndView("platform/list");
		result.addObject("platforms", platforms);
		result.addObject("requestURI", "platform/list.do");
		
		return result;
	}
	
	@RequestMapping(value = "/listKeyword1", method = RequestMethod.POST)
	public ModelAndView listKeyword1(@RequestParam String keyword) {

		ModelAndView result;
		Collection<Platform> platforms;
		try {
			platforms = platformService.findByKeyword(keyword);
			result = new ModelAndView("platform/list");
			result.addObject("requestURI", "platform/listKeyword.do");
			result.addObject("platforms", platforms);
		} catch (Throwable error) {
			result = new ModelAndView("redirect:list.do");
		}
	
		return result;
	}

}
