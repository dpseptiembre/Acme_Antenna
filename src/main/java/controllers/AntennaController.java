
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AntennaService;
import domain.Antenna;
import services.UserService;

@Controller
@RequestMapping("/antenna")
public class AntennaController extends AbstractController {

	public AntennaController() {
		super();
	}


	@Autowired
	private AntennaService	antennaService;
	@Autowired
	private UserService userService;


	protected static ModelAndView createEditModelAndView(final Antenna antenna) {
		ModelAndView result;

		result = AntennaController.createEditModelAndView(antenna, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Antenna antenna, final String message) {
		ModelAndView result;

		result = new ModelAndView("antenna/edit");
		result.addObject("antenna", antenna);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView antennasList() {

		ModelAndView result;
		Collection<Antenna> antennas;
		antennas = this.antennaService.findAll();
		result = new ModelAndView("antenna/list");
		result.addObject("antennas", antennas);
		result.addObject("requestURI", "antenna/list.do");
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;

		final Antenna antenna = this.antennaService.create();
		result = AntennaController.createEditModelAndView(antenna);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int antennaId) {
		ModelAndView result;
		Antenna antenna;

		antenna = this.antennaService.findOne(antennaId);
		Assert.notNull(antenna);
		Assert.isTrue(userService.findByPrincipal().getAntennas().contains(antenna));
		result = AntennaController.createEditModelAndView(antenna);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Antenna antenna, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = AntennaController.createEditModelAndView(antenna);
		else
			try {
				this.antennaService.save(antenna);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = AntennaController.createEditModelAndView(antenna, "antenna.commit.error");
			}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Antenna antenna) {
		ModelAndView result;
		try {
			this.antennaService.delete(antenna);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = AntennaController.createEditModelAndView(antenna, "antenna.commit.error");
		}
		return result;
	}

}
