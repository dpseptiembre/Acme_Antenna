
package controllers;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CommentService;
import services.TutorialService;
import domain.Comment;
import domain.Tutorial;

@Controller
@RequestMapping("/comment")
public class CommentController extends AbstractController {

	public CommentController() {
		super();
	}


	@Autowired
	private CommentService	commentService;
	@Autowired
	private TutorialService	tutorialService;


	protected static ModelAndView createEditModelAndView(final Comment comment) {
		ModelAndView result;

		result = CommentController.createEditModelAndView(comment, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Comment comment, final String message) {
		ModelAndView result;

		result = new ModelAndView("comment/edit");
		result.addObject("comment", comment);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView commentsList(@RequestParam final int commentId) {

		ModelAndView result;
		Collection<Comment> comments;
		Comment comment;
		comment = this.commentService.findOne(commentId);
		comments = comment.getReplies();
		result = new ModelAndView("comment/list");
		result.addObject("comments", comments);
		result.addObject("requestURI", "comment/list.do");

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int tutorialId) {

		ModelAndView result;

		final Comment comment = this.commentService.create();
		comment.setObjetiveId(tutorialId);
		result = CommentController.createEditModelAndView(comment);

		return result;
	}

	@RequestMapping(value = "/reply", method = RequestMethod.GET)
	public ModelAndView reply(@RequestParam final int commentId) {

		ModelAndView result;

		final Comment comment = this.commentService.create();
		comment.setObjetiveId(commentId);
		result = CommentController.createEditModelAndView(comment);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int commentId) {
		ModelAndView result;
		Comment comment;

		comment = this.commentService.findOne(commentId);
		Assert.notNull(comment);
		result = CommentController.createEditModelAndView(comment);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Comment comment, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = CommentController.createEditModelAndView(comment);
		else
			try {
				final Tutorial tutorial = this.tutorialService.findOne(comment.getObjetiveId());
				if (tutorial != null) {
					comment.setMoment(new Date(System.currentTimeMillis() - 1000));
					comment.setTutorial(tutorial);
					this.commentService.save(comment);
					comment.setObjetiveId(0);
					result = new ModelAndView("tutorial/list");
				} else {
					final Comment aux = this.commentService.findOne(comment.getObjetiveId());
					comment.setMoment(new Date(System.currentTimeMillis() - 1000));

					final Collection<Comment> aux2 = aux.getReplies();
					aux2.add(comment);
					aux.setReplies(aux2);
					this.commentService.save(aux);
					result = new ModelAndView("tutorial/list");
				}

			} catch (final Throwable oops) {
				result = CommentController.createEditModelAndView(comment, "comment.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Comment comment) {
		ModelAndView result;
		try {
			final Comment aux = this.commentService.findOne(comment.getObjetiveId());
			if (aux != null) {
				final Collection<Comment> aux2 = aux.getReplies();
				aux2.remove(comment);
				aux.setReplies(aux2);
				this.commentService.save(aux);
				this.commentService.delete(comment);
			} else
				this.commentService.delete(comment);
			result = new ModelAndView("tutorial/list");
		} catch (final Throwable oops) {
			result = CommentController.createEditModelAndView(comment, "comment.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView commentDelete(@RequestParam final int commentId) {

		ModelAndView result;
		Comment comment;

		comment = this.commentService.findOne(commentId);
		this.commentService.delete(comment);

		Collection<Comment> comments;
		comments = this.commentService.findAll();
		result = new ModelAndView("comment/list");
		result.addObject("comments", comments);
		result.addObject("requestURI", "comment/list.do");

		return result;
	}
}
