package converters;

import domain.Tutorial;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class TutorialToStringConverter implements Converter<Tutorial, String> {

    @Override
    public String convert(Tutorial tutorial) {
        Assert.notNull(tutorial);
        String result;
        result = String.valueOf(tutorial.getId());
        return result;
    }

}