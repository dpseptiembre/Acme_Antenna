package converters;

import domain.Satellite;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class SatelliteToStringConverter implements Converter<Satellite, String> {

    @Override
    public String convert(Satellite satellite) {
        Assert.notNull(satellite);
        String result;
        result = String.valueOf(satellite.getId());
        return result;
    }

}
