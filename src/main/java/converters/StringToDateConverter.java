
package converters;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

public class StringToDateConverter implements Converter<String, Date> {

	@SuppressWarnings("deprecation")
	@Override
	public Date convert(final String source) {
		Date result;
		final SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");
		try {
			if (StringUtils.isEmpty(source))
				result = null;
			else
				result = myFormat.parse(source);
			//	            result = new Date(source);
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return result;
	}
}
