package converters;

import domain.Subscription;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class SubscriptionToStringConverter implements Converter<Subscription, String> {

    @Override
    public String convert(Subscription subscription) {
        Assert.notNull(subscription);
        String result;
        result = String.valueOf(subscription.getId());
        return result;
    }

}

