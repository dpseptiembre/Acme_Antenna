package services;

import java.util.Collection;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.TutorialRepository;
import domain.Comment;
import domain.Tutorial;

@Service
@Transactional
public class TutorialService {

	@Autowired
	private TutorialRepository tutorialRepository;

	@Autowired
	private CommentService	commentService;

	public TutorialService() {
		super();
	}

	public Tutorial create() {
		return new Tutorial();
	}

	public Tutorial findOne(final int tutorialId) {
		Tutorial result;

		result = this.tutorialRepository.findOne(tutorialId);

		return result;
	}

	public Collection<Tutorial> findAll() {
		Collection<Tutorial> result;

		result = this.tutorialRepository.findAll();

		return result;
	}

	public Tutorial save(final Tutorial tutorial) {
		Assert.notNull(tutorial);
		return this.tutorialRepository.save(tutorial);
	}

	public void delete(final Tutorial tutorial) {
		Assert.notNull(tutorial);
		Assert.isTrue(this.tutorialRepository.exists(tutorial.getId()));
		this.tutorialRepository.delete(tutorial);
	}


	public void deleteTutorialWithTabooWord(int tutorialId) {
		Tutorial tutorial;
		tutorial = tutorialRepository.findOne(tutorialId);
		for(Comment c: tutorial.getComments()){ 
		commentService.delete2(c);
		}
		tutorialRepository.delete(tutorialId);
		
	}
}
