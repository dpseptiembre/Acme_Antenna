
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.AntennaRepository;
import domain.Antenna;
import domain.User;

@Service
@Transactional
public class AntennaService {

	@Autowired
	private AntennaRepository	antennaRepository;
	@Autowired
	private UserService	userService;

	public AntennaService() {
		super();
	}

	public Antenna create() {
		User user;
		Antenna antenna;
		user=userService.findByPrincipal();
		
		antenna=new Antenna();
		antenna.setUser(user);
		
		return antenna;
	}

	public Antenna findOne(final int AntennaId) {
		Antenna result;

		result = this.antennaRepository.findOne(AntennaId);

		return result;
	}

	public Collection<Antenna> findAll() {
		Collection<Antenna> result;

		result = this.antennaRepository.findAll();

		return result;
	}

	public Antenna save(final Antenna Antenna) {
		Assert.notNull(Antenna);
		return this.antennaRepository.save(Antenna);
	}

	public void delete(final Antenna Antenna) {
		Assert.notNull(Antenna);
		Integer antennaId;
		antennaId=Antenna.getId();
		Assert.isTrue(this.antennaRepository.exists(antennaId));
		
		this.antennaRepository.delete(Antenna);
	}

	public Collection<Antenna> findByUserId(int userId) {
		Collection<Antenna> result;
		result= antennaRepository.findByUserId(userId);
		return result;
	}
}
