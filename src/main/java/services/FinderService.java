
package services;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.FinderRepository;
import domain.Finder;
import domain.Platform;
import domain.Satellite;

@Service
@Transactional
public class FinderService {

	// Managed repository -----------------------------------------------------

	@Autowired
	private FinderRepository	finderRepository;
	@Autowired
	private SatelliteService	satelliteService;
	@Autowired
	private PlatformService		platformService;


	// Supporting services ----------------------------------------------------

	// Constructors -----------------------------------------------------------

	public FinderService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------
	public Finder create() {
		return new Finder();
	}

	public Collection<Finder> findAll() {
		Collection<Finder> result;

		result = this.finderRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Finder findOne(final int finderId) {
		Assert.isTrue(finderId != 0);
		Finder result;
		result = this.finderRepository.findOne(finderId);
		Assert.notNull(result);

		return result;
	}

	public Finder save(final Finder finder) {
		Assert.notNull(finder);

		Finder result;

		result = this.finderRepository.save(finder);

		return result;
	}

	public void delete(final Finder finder) {
		Assert.notNull(finder);
		Assert.isTrue(finder.getId() != 0);
		Assert.isTrue(this.finderRepository.exists(finder.getId()));

		this.finderRepository.delete(finder);
	}

	// Other business methods -------------------------------------------------

	public Set<Satellite> finderSatellite(final String keyword) {
		final Set<Satellite> results = new HashSet<>(this.satelliteService.findAll());
		Set<Satellite> aux = new HashSet<>();
		for (final Satellite satellite : results) {
			if (satellite.getDescription().contains(keyword))
				aux.add(satellite);
			if (satellite.getName().contains(keyword))
				aux.add(satellite);
		}
		if (aux.isEmpty())
			aux = new HashSet<>(this.satelliteService.findAll());
		return aux;
	}

	public Set<Platform> finderPlatform(final String keyword) {
		final Set<Platform> results = new HashSet<>(this.platformService.findAll());
		Set<Platform> aux = new HashSet<>();
		for (final Platform platform : results) {
			if (platform.getDescription().contains(keyword))
				aux.add(platform);
			if (platform.getName().contains(keyword))
				aux.add(platform);
		}
		if (aux.isEmpty())
			aux = new HashSet<>(this.platformService.findAll());
		return aux;
	}

}
