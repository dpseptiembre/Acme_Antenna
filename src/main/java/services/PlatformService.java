
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.PlatformRepository;
import domain.Platform;

@Service
@Transactional
public class PlatformService {

	@Autowired
	private PlatformRepository	platformRepository;


	public PlatformService() {
		super();
	}

	public Platform create() {
		return new Platform();
	}

	public Platform findOne(final int PlatformId) {
		Platform result;

		result = this.platformRepository.findOne(PlatformId);

		return result;
	}

	public Collection<Platform> findAll() {
		Collection<Platform> result;

		result = this.platformRepository.findAll();

		return result;
	}

	public Platform save(final Platform Platform) {
		Assert.notNull(Platform);
		return this.platformRepository.save(Platform);
	}

	public void delete(final Platform Platform) {
		Assert.notNull(Platform);
		Assert.isTrue(this.platformRepository.exists(Platform.getId()));
		this.platformRepository.delete(Platform);
	}

	public Collection<Platform> findBySatelliteId(Integer satelliteId) {
		Assert.notNull(satelliteId);
		Collection<Platform> result;
		result= platformRepository.findBySatelliteId(satelliteId);
		Assert.notNull(result);
		return result;
	}

	public Collection<Platform> findByKeyword(String keyword) {
		Collection<Platform> result;

		result = platformRepository.findByKeyword(keyword);

		return result;
	}
}
