
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.UserRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;
import domain.Actor;
import domain.User;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository		userRepository;

	@Autowired
	private UserAccountService	userAccountService;


	public UserService() {
		super();
	}

	public User create() {
		final User res = new User();
		this.authoritySet(res);
		return res;

	}

	public User findOne(final int userId) {
		Assert.isTrue(userId != 0);
		User result;
		result = this.userRepository.findOne(userId);
		Assert.notNull(result);

		return result;

	}

	public Collection<User> findAll() {
		Collection<User> result;

		result = this.userRepository.findAll();
		Assert.notNull(result);

		return result;

	}

	public User save(final User user) {
		Assert.notNull(user);

		User result;

		result = this.userRepository.save(user);

		return result;

	}

	public void delete(final User user) {
		Assert.notNull(user);
		Assert.isTrue(user.getId() != 0);
		Assert.isTrue(this.userRepository.exists(user.getId()));

		this.userRepository.delete(user);

	}

	// Other business methods -------------------------------------------------

	public UserAccount findUserAccount(final User user) {
		Assert.notNull(user);

		UserAccount result;

		result = this.userAccountService.findByUser(user);

		return result;
	}

	public User findByPrincipal() {
		User result;
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);
		return result;
	}

	public User findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		User result;

		result = this.userRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public Actor registerAsUser(final User u) {
		Assert.notNull(u);
		u.getUserAccount().setUsername(u.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
		u.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(u.getUserAccount());
		u.setUserAccount(userAccount);
		u.setBan(false);
		u.getUserAccount().setIsAccountNonLocked(true);
		Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
		final User resu = this.userRepository.save(u);
		return resu;
	}

	private void authoritySet(final User user) {
		Assert.notNull(user);
		final Authority autoh = new Authority();
		autoh.setAuthority(Authority.USER);
		final Collection<Authority> authorities = new ArrayList<>();
		authorities.add(autoh);
		final UserAccount res1 = new UserAccount();
		res1.setAuthorities(authorities);
		//final UserAccount userAccount = this.userAccountService.save(res1);
		user.setUserAccount(res1);
		Assert.notNull(user.getUserAccount().getAuthorities(), "authorities null al registrar");
	}

}
