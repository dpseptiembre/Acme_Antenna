package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.CommentRepository;
import domain.Comment;

@Service
@Transactional
public class CommentService {

	@Autowired
	private CommentRepository commentRepository;

	public CommentService() {
		super();
	}

	public Comment create() {
		return new Comment();
	}

	public Comment findOne(final int CommentId) {
		Comment result;

		result = this.commentRepository.findOne(CommentId);

		return result;
	}

	public Collection<Comment> findAll() {
		Collection<Comment> result;

		result = this.commentRepository.findAll();

		return result;
	}

	public Comment save(final Comment Comment) {
		Assert.notNull(Comment);
		return this.commentRepository.save(Comment);
	}

	public void delete(final Comment Comment) {
		Assert.notNull(Comment);
		Assert.isTrue(this.commentRepository.exists(Comment.getId()));
		this.commentRepository.delete(Comment);
	}


	public void delete2(Comment comment) {
		Assert.notNull(comment);
		Collection<Comment> comments;
		Assert.isTrue(this.commentRepository.exists(comment.getId()));
		comments = comment.getReplies();
		for (Comment c : comments) {
			commentRepository.delete(c);
			delete2(c);

		}
		this.commentRepository.delete(comment);

	}
}
