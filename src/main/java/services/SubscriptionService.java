
package services;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.SubscriptionRepository;
import domain.Subscription;
import domain.User;

@Service
@Transactional
public class SubscriptionService {

	@Autowired
	private SubscriptionRepository	subscriptionRepository;
	@Autowired
	private UserService userService;


	public SubscriptionService() {
		super();
	}

	public Subscription create() {
		Subscription result;
		User user;
		result= new Subscription();
		user=userService.findByPrincipal();
		result.setUser(user);
		
		Date date;
		
		date=new Date();
		
		
		String keyCode;
		keyCode=generateKeyCode();
		result.setKeyCode(keyCode);
		result.setStartDate(date);
		return result;
	}

	public Subscription findOne(final int SubscriptionId) {
		Subscription result;

		result = this.subscriptionRepository.findOne(SubscriptionId);

		return result;
	}

	public Collection<Subscription> findAll() {
		Collection<Subscription> result;

		result = this.subscriptionRepository.findAll();

		return result;
	}

	public Subscription save(final Subscription Subscription) {
		Assert.notNull(Subscription);
		return this.subscriptionRepository.save(Subscription);
	}

	public void delete(final Subscription Subscription) {
		Assert.notNull(Subscription);
		Assert.isTrue(this.subscriptionRepository.exists(Subscription.getId()));
		this.subscriptionRepository.delete(Subscription);
	}

	public Collection<Subscription> findByUserId(int userId) {
		Collection<Subscription> result;
		result= subscriptionRepository.findByUserId(userId);
		return result;
	}
	
	protected static String generateKeyCode(){

		
		 String uuid;
		 uuid= UUID.randomUUID().toString();
		 String result;
		 result=uuid.replaceAll("-", "");
	      return result;
	}
}