
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.User;

@Service
@Transactional
public class ActorService {

	@Autowired
	private ActorRepository	actorRepository;


	public ActorService() {
		super();
	}

	public Actor create() {
		return new Actor();
	}

	public Actor findOne(final int actorId) {
		Actor result;

		result = this.actorRepository.findOne(actorId);

		return result;
	}

	public Collection<Actor> findAll() {
		Collection<Actor> result;

		result = this.actorRepository.findAll();

		return result;
	}

	public Actor save(final Actor actor) {
		Assert.notNull(actor);
		return this.actorRepository.save(actor);
	}

	public void delete(final Actor actor) {
		Assert.notNull(actor);
		Assert.isTrue(this.actorRepository.exists(actor.getId()));
		this.actorRepository.delete(actor);
	}

	public Collection<Actor> findBan() {
		Collection<Actor> result;
		
		result=actorRepository.findBan();
		
		return result;
	}
	
	public Collection<Actor> findAllExceptAdmin(){
		Collection<Actor> actors;
		Collection<Actor> result;
		actors= actorRepository.findAll();
		result=actorRepository.findAll();
		Authority au = new Authority();
		
		au.setAuthority("ADMIN");
		
		for(Actor a:actors){
			if(a.getUserAccount().getAuthorities().contains(au)){
				result.remove(a);
			}
		}
		
		return result;
		
	}
	
	public Actor findByPrincipal() {
		Actor result;
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);
		return result;
	}
	
	public Actor findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Actor result;

		result = this.actorRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

}
