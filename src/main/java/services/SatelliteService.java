
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.SatelliteRepository;
import domain.Satellite;

@Service
@Transactional
public class SatelliteService {

	@Autowired
	private SatelliteRepository	satelliteRepository;


	public SatelliteService() {
		super();
	}

	public Satellite create() {
		return new Satellite();
	}

	public Satellite findOne(final int SatelliteId) {
		Satellite result;

		result = this.satelliteRepository.findOne(SatelliteId);

		return result;
	}

	public Collection<Satellite> findAll() {
		Collection<Satellite> result;

		result = this.satelliteRepository.findAll();

		return result;
	}

	public Satellite save(final Satellite Satellite) {
		Assert.notNull(Satellite);
		return this.satelliteRepository.save(Satellite);
	}

	public void delete(final Satellite Satellite) {
		Assert.notNull(Satellite);
		Assert.isTrue(this.satelliteRepository.exists(Satellite.getId()));
		this.satelliteRepository.delete(Satellite);
	}

	public Collection<Satellite> findByPlatformId(Integer platformId) {
		Assert.notNull(platformId);
		Collection<Satellite> result;
		result= satelliteRepository.findByPlatformId(platformId);
		Assert.notNull(result);
		return result;
	}

	public Collection<Satellite> findByKeyword(String keyword) {
		Collection<Satellite> result;

		result = satelliteRepository.findByKeyword(keyword);

		return result;
	}
}
