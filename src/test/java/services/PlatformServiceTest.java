
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Platform;
import domain.Satellite;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class PlatformServiceTest extends AbstractTest {

	@Autowired
	private PlatformService		platformService;
	@Autowired
	private SatelliteService	satelliteService;


	@Test
	public void platformBroadcast() {
		final List<Satellite> satellites = (List<Satellite>) this.satelliteService.findAll();
		final Satellite satellite = satellites.get(0);
		final List<Platform> platforms = (List<Platform>) this.platformService.findBySatelliteId(satellite.getId());
		Assert.notNull(platforms);
	}

	@Test(expected = IllegalArgumentException.class)
	public void platformBroadcastNegative() {
		final Satellite satellite = this.satelliteService.create();
		final Satellite res = this.satelliteService.save(satellite);
		final List<Platform> platforms = (List<Platform>) this.platformService.findBySatelliteId(res.getId());
		Assert.notEmpty(platforms);
	}
}
