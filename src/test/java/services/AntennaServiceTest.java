
package services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Antenna;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AntennaServiceTest extends AbstractTest {

	@Autowired
	private AntennaService	antennaService;
	@Autowired
	private UserService		userService;


	@Test
	public void driver() {
		final List<Antenna> antennaList = new ArrayList<>();

		final Object testingData[][] = {
			//01 - EMPTY LIST
			/**
			 * Description: An actor who is authenticated as a user must be able to: Manage his or her antennas, which includes registering them, editing them, deleting them, and listing them.
			 * Return: TRUE
			 * Postcondition: The antennas list is shown empty.
			 */
			{
				"user1", antennaList.isEmpty(), null
			},
			//02 - REGULAR USER WITH AN ANTENANAS
			/**
			 * Description: An actor who is authenticated as a user must be able to: Manage his or her antennas, which includes registering them, editing them, deleting them, and listing them.
			 * Precondition: The user is a user with an antennas list with one item.
			 * Return: TRUE
			 * Postcondition: The antennas list is shown with one single item.
			 */
			{
				"user2", antennaList.size() == 1, AssertionFailedError.class
			},
			//03 - ALL ANTENNAS
			/**
			 * Description: An actor who is authenticated as a user must be able to: Manage his or her antennas, which includes registering them, editing them, deleting them, and listing them.
			 * Precondition: The user is a user with an antennas list with more than one item.
			 * Return: TRUE
			 * Postcondition: The antennas list is shown with multiple items.
			 */
			{
				"user3", antennaList.size() >= 2, AssertionFailedError.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.template((String) testingData[i][0], (Boolean) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	protected void template(final String user, final Boolean ass, final Class<?> exc) {
		Class<?> caught;
		caught = null;
		try {
			this.authenticate(user);
			final List<Antenna> antennas = new ArrayList<>(this.antennaService.findAll());
			final User u = new ArrayList<>(this.userService.findAll()).get(0);
			final Antenna antenna = antennas.get(0);
			Assert.assertNotNull(antenna.getModel());
			Assert.assertTrue(ass);
			this.authenticate(null);
		} catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(exc, caught);
	}

	@Test
	public void createAntenna() {
		this.authenticate("user1");
		final Antenna antenna = this.antennaService.create();
		antenna.setAzimuth(10);
		antenna.setElevation(20);
		antenna.setModel("Antena");
		antenna.setQuality(50);
		antenna.setSatelliteToward("Torre 1");
		final Antenna res = this.antennaService.save(antenna);
		Assert.assertNotNull(res);
		this.unauthenticate();
	}

	@Test(expected = AssertionError.class)
	public void createAntennaNegative() {
		this.authenticate("user1");
		final Antenna antenna = this.antennaService.create();
		antenna.setAzimuth(10);
		antenna.setElevation(20);
		antenna.setModel("Antena");
		antenna.setQuality(50);
		antenna.setSatelliteToward("Torre 1");
		final Antenna res = this.antennaService.save(antenna);
		Assert.assertNotNull(res.getCoordinates());
		this.unauthenticate();
	}

	@Test
	public void editAntenna() {
		this.authenticate("user1");
		final Antenna a = this.antennaService.create();
		final Antenna res = this.antennaService.save(a);
		Assert.assertNotNull(res);
		this.unauthenticate();
	}

	@Test
	public void removeAntenna() {
		this.authenticate("user1");
		final List<Antenna> antennas = new ArrayList<>(this.antennaService.findAll());
		final Integer initialSize = antennas.size();
		antennas.remove(0);
		final Integer finalSize = antennas.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

}
