
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Platform;
import domain.Satellite;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class FinderServiceTest extends AbstractTest {

	@Autowired
	private FinderService	finderService;


	@Test
	public void finder() {
		final String key = "a";
		final Collection<Satellite> res = this.finderService.finderSatellite(key);
		final Collection<Platform> aux = this.finderService.finderPlatform(key);
		org.springframework.util.Assert.notEmpty(res);
		org.springframework.util.Assert.notEmpty(aux);
	}
	@Test
	public void finderRandom() {
		final String key = "perri";
		final Collection<Satellite> res = this.finderService.finderSatellite(key);
		final Collection<Platform> aux = this.finderService.finderPlatform(key);
		org.springframework.util.Assert.notEmpty(res);
		org.springframework.util.Assert.notEmpty(aux);
	}
	@Test
	public void finderRandomNumbers() {
		final String key = "perri";
		final Collection<Satellite> res = this.finderService.finderSatellite(key);
		final Collection<Platform> aux = this.finderService.finderPlatform(key);
		org.springframework.util.Assert.notEmpty(res);
		org.springframework.util.Assert.notEmpty(aux);
	}
	@Test
	public void finderRandom2() {
		final String key = "dhhfd zdhg";
		final Collection<Satellite> res = this.finderService.finderSatellite(key);
		final Collection<Platform> aux = this.finderService.finderPlatform(key);
		org.springframework.util.Assert.notEmpty(res);
		org.springframework.util.Assert.notEmpty(aux);
	}
	@Test
	public void finderEmpty() {
		final String key = "";
		final Collection<Satellite> res = this.finderService.finderSatellite(key);
		final Collection<Platform> aux = this.finderService.finderPlatform(key);
		org.springframework.util.Assert.notEmpty(res);
		org.springframework.util.Assert.notEmpty(aux);
	}

}
