
package services;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;
import domain.CreditCard;
import domain.Subscription;
import domain.Tutorial;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class UserServiceTest extends AbstractTest {

	@Autowired
	private UserService			userService;
	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
	private SubscriptionService	subscriptionService;
	@Autowired
	private TutorialService		tutorialService;

	@Test
	public void registerAsHandyworker() {
		final User u = this.userService.create();
		Assert.notNull(u);
		u.getUserAccount().setUsername("kkkkkk");
		u.setName("kkkkkk");
		u.setSurname("sdfsdf");
		u.setEmail("perri@gjail.com");
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword("kkkkkk", null);
		u.getUserAccount().setPassword(hash);
		org.junit.Assert.assertNotNull(u);
	}

	@Test(expected = AssertionError.class)
	public void registerAsHAndyworkerNegative() {
		final User u = this.userService.create();
		u.setName("perrito");
		u.getUserAccount().setUsername("perrito");
		u.getUserAccount().setPassword("perrito");
		Assert.notNull(u);
		u.getUserAccount().setUsername(u.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
		u.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(u.getUserAccount());
		u.setUserAccount(userAccount);
		Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
		final User resu = this.userService.save(u);
		org.junit.Assert.assertNotNull(resu.getEmail());
	}

	@Test
	public void subscription() {
		this.authenticate("user1");
		final CreditCard c = null;
		final Subscription s = this.subscriptionService.create();
		s.setEndDate(new Date());
		s.setStartDate(new Date());
		s.setCreditCard(c);
		final Subscription res = this.subscriptionService.save(s);
		Assert.notNull(res.getKeyCode());
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void subscriptionNegative() {
		this.authenticate("user1");
		final CreditCard c = null;
		final Subscription s = this.subscriptionService.create();
		s.setEndDate(new Date());
		s.setStartDate(new Date());
		s.setCreditCard(c);
		final Subscription res = this.subscriptionService.save(s);
		Assert.notNull(res.getCreditCard());
		this.unauthenticate();
	}

	@Test
	public void tutorialCommentList() {
		this.authenticate("user1");
		final List<Tutorial> tutorials = (List<Tutorial>) this.tutorialService.findAll();
		final Tutorial tutorial = tutorials.get(0);
		Assert.notNull(tutorial);
		Assert.notNull(tutorial.getComments());
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void tutorialCommentListNegative() {
		this.authenticate("user1");
		final List<Tutorial> tutorials = (List<Tutorial>) this.tutorialService.findAll();
		final Tutorial tutorial = tutorials.get(80);
		Assert.notNull(tutorial);
		Assert.notNull(tutorial.getComments());
		this.unauthenticate();
	}


}
