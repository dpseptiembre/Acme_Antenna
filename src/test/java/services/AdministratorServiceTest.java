
package services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;

import domain.Comment;
import domain.Tutorial;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AdministratorServiceTest extends AbstractTest {

	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private UserService				userService;
	@Autowired
	private TutorialService			tutorialService;
	@Autowired
	private CommentService			commentService;


	@Test
	public void deleteTutorial() {
		this.authenticate("administrator1");
		final List<Tutorial> tutorials = (List<Tutorial>) this.tutorialService.findAll();
		final Tutorial tutorial = tutorials.get(0);
		final int id = tutorial.getId();
		this.tutorialService.delete(tutorial);
		Assert.assertNull(this.tutorialService.findOne(id));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void deleteTutorialNegative() {
		this.authenticate("administrator1");
		final List<Tutorial> tutorials = (List<Tutorial>) this.tutorialService.findAll();
		final Tutorial tutorial = tutorials.get(88);
		this.tutorialService.delete(tutorial);
	}

	@Test
	public void deleteComment() {
		this.authenticate("administrator1");
		final List<Comment> comments = (List<Comment>) this.commentService.findAll();
		final Comment comment = comments.get(0);
		final int id = comment.getId();
		this.commentService.delete(comment);
		Assert.assertNull(this.commentService.findOne(id));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void deleteCommentNegative() {
		this.authenticate("administrator1");
		final List<Comment> comments = (List<Comment>) this.commentService.findAll();
		final Comment comment = comments.get(80);
		this.commentService.delete(comment);
	}

	@Test
	public void banUser() {
		this.authenticate("administrator1");
		final List<User> users = new ArrayList<>(this.userService.findAll());
		final User user = users.get(0);
		this.administratorService.ban(user);
		Assert.assertTrue(user.getBan());
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void banUserNegative() {
		this.authenticate("administrator1");
		final List<User> users = new ArrayList<>(this.userService.findAll());
		final User user = users.get(80);
		this.administratorService.ban(user);
		Assert.assertTrue(user.getBan());
		this.unauthenticate();
	}

	@Test
	public void unBan() {
		this.authenticate("administrator1");
		final List<User> users = new ArrayList<>(this.userService.findAll());
		final User user = users.get(0);
		this.administratorService.unban(user);
		Assert.assertFalse(user.getBan());
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void unBanNegative() {
		this.authenticate("administrator1");
		final List<User> users = new ArrayList<>(this.userService.findAll());
		final User user = users.get(80);
		this.administratorService.unban(user);
		Assert.assertFalse(user.getBan());
		this.unauthenticate();
	}

}
