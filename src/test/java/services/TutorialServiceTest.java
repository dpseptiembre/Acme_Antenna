
package services;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Tutorial;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class TutorialServiceTest extends AbstractTest {

	@Autowired
	private TutorialService	tutorialService;


	@Test
	public void listTutorials() {
		final List<Tutorial> tutorials = (List<Tutorial>) this.tutorialService.findAll();
		Assert.notNull(tutorials);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void listTutorialsNegative() {
		final List<Tutorial> tutorials = (List<Tutorial>) this.tutorialService.findAll();
		Assert.notNull(tutorials.get(80));
	}

	@Test
	public void createTutorial() {
		this.authenticate("user1");
		final Tutorial tutorial = this.tutorialService.create();
		tutorial.setMoment(new Date());
		tutorial.setText("Hola");
		tutorial.setTitle("Titulo");
		final Tutorial res = this.tutorialService.save(tutorial);
		Assert.notNull(res);
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void createTutorialNegative() {
		this.authenticate("user1");
		final Tutorial tutorial = this.tutorialService.create();
		tutorial.setMoment(new Date());
		tutorial.setText("Hola");
		tutorial.setTitle("Titulo");
		final Tutorial res = this.tutorialService.save(tutorial);
		Assert.notNull(res.getComments());
		this.unauthenticate();
	}

	@Test
	public void editTutorial() {
		this.authenticate("user1");
		final Tutorial a = this.tutorialService.create();
		final Tutorial res = this.tutorialService.save(a);
		Assert.notNull(res);
		this.unauthenticate();
	}

}
