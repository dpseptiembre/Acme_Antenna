
package services;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Platform;
import domain.Satellite;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class SatelliteServiceTest extends AbstractTest {

	@Autowired
	private PlatformService		platformService;
	@Autowired
	private SatelliteService	satelliteService;


	@Test
	public void satelliteBroadcast() {
		final List<Platform> platforms = (List<Platform>) this.platformService.findAll();
		final Platform platform = platforms.get(0);
		final List<Satellite> satellites = (List<Satellite>) this.satelliteService.findByPlatformId(platform.getId());
		Assert.notNull(satellites);
	}

	@Test(expected = ConstraintViolationException.class)
	public void platformBroadcastNegative() {
		final Platform platform = this.platformService.create();
		final Platform res = this.platformService.save(platform);
		final List<Satellite> satellites = (List<Satellite>) this.satelliteService.findByPlatformId(res.getId());
		Assert.notEmpty(satellites);
	}

}
