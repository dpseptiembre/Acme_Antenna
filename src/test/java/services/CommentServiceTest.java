
package services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class CommentServiceTest extends AbstractTest {

	@Autowired
	private CommentService	commentService;


	@Test
	public void createComment() {
		this.authenticate("user1");
		final Comment comment = this.commentService.create();
		comment.setMoment(new Date(System.currentTimeMillis() - 1000));
		comment.setText("Hola");
		comment.setTitle("Titulo");
		final Comment res = this.commentService.save(comment);
		Assert.assertNotNull(res);
		this.unauthenticate();
	}

	@Test(expected = AssertionError.class)
	public void createCommentNegative() {
		this.authenticate("user1");
		final Comment comment = this.commentService.create();
		comment.setMoment(new Date(System.currentTimeMillis() - 1000));
		comment.setText("Hola");
		comment.setTitle("Titulo");
		final Comment res = this.commentService.save(comment);
		Assert.assertNotNull(res.getReplies());
		this.unauthenticate();
	}

	@Test
	public void replyComment() {
		this.authenticate("user1");
		final List<Comment> comments = (List<Comment>) this.commentService.findAll();
		final Comment res = comments.get(0);
		final Comment comment = this.commentService.create();
		comment.setMoment(new Date(System.currentTimeMillis() - 1000));
		comment.setText("Hola");
		comment.setTitle("Titulo");
		final List<Comment> aux = new ArrayList<Comment>();
		aux.add(comment);
		res.setReplies(aux);
		Assert.assertNotNull(res.getReplies());
		this.unauthenticate();
	}

	@Test(expected = AssertionError.class)
	public void replyCommentNegative() {
		this.authenticate("user1");
		final List<Comment> comments = (List<Comment>) this.commentService.findAll();
		final Comment res = comments.get(0);
		final Comment comment = this.commentService.create();
		comment.setMoment(new Date(System.currentTimeMillis() - 1000));
		comment.setText("Hola");
		comment.setTitle("Titulo");
		final Comment c = this.commentService.save(comment);
		final List<Comment> aux = new ArrayList<Comment>();
		aux.add(c);
		res.setReplies(aux);
		Assert.assertNotNull(c.getReplies());
		this.unauthenticate();
	}

}
